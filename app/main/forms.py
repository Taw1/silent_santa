from ..models import Card
from flask_wtf import Form
from wtforms import BooleanField, SubmitField, IntegerField, validators, TextAreaField, StringField
from wtforms.validators import Required, Length, Email



class BoxForm(Form):
    allow_suggestions = BooleanField('Allow suggestions?')
    suggestion = TextAreaField('Type your suggestions here', [validators.length(max=255, message='Maximum 255 symbols')],
                               render_kw={'disabled': 'true'})
    money_restrictions = IntegerField('Money restrictions',
                                      [validators.NumberRange(min=0, message='Must be grater then 0')],
                                      default=0)
    submit = SubmitField('Create box')

class CardForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                             Email()])
    username = StringField('Username', validators=[
        Required(), Length(1, 64)])
    suggestion = TextAreaField('Type your suggestions here')
    submit = SubmitField('Create card')
