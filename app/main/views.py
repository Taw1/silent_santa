from . import main
from flask import render_template, url_for, redirect, flash, request
from flask_login import current_user, login_required
from .forms import BoxForm, CardForm
from ..models import Box, Card
from .. import login_manager
from ..auth import auth


@login_manager.unauthorized_handler
def handle_needs_login():
    flash("You have to be logged in to access this page.")
    return redirect(url_for('auth.login', next=request.endpoint[5:]))


@main.route('/')
def index():
    return render_template('index.html')


@main.context_processor
@auth.context_processor
def show_boxes():
    if current_user.is_authenticated:
        boxes_a = [box.box_hash for box in Box.query.filter_by(user_id=current_user.id).all()]
        boxes_r = [Box.query.filter_by(id=card.box_id).one().box_hash
                   for card in Card.query.filter_by(email=current_user.email).all()
                   if Box.query.filter_by(id=card.box_id).one().user_id != current_user.id]
        return dict(boxes_a=boxes_a, boxes_r=boxes_r)
    return dict()


@main.route('/create_box', methods=['GET', 'POST'])
@login_required
def create_box():
    form = BoxForm()
    if form.validate_on_submit():
        box_hash = Box.create_box(user_id=current_user.id,
                                  user_email=current_user.email,
                                  allow_suggestions=form.allow_suggestions.data,
                                  suggestions=form.suggestion.data,
                                  money_rest=form.money_restrictions.data
                                  )

        return redirect(url_for('.box', hash=box_hash))
    return render_template('create_box.html', form=form)


@main.route('/box/<hash>')
def box(hash):
    box = Box.query.filter_by(box_hash=hash).one()
    cards = Card.query.filter_by(box_id=box.id).all()
    box_creator = True if current_user.is_authenticated and current_user.id == box.user_id else False
    money_rest = box.money_rest
    return render_template('box.html',
                           cards=cards,
                           box=box,
                           box_creator=box_creator)


@main.route('/box/<hash>/add_card/', methods=['GET', 'POST'])
def add_card(hash):
    form = CardForm()
    allow_suggestions = Box.query.filter_by(box_hash=hash).one().allow_suggestions
    if form.validate_on_submit():
        data = Card.create_card(box_hash=hash,
                                email=form.email.data,
                                username=form.username.data,
                                suggestions=form.suggestion.data)
        if data:
            flash(data)
            return render_template('add_card.html', form=form)
        flash('Card successfully added for {}'.format(form.username.data))
        return redirect(url_for('.box', hash=hash))
    return render_template('add_card.html', form=form, hash=hash, allow_suggestions=allow_suggestions)


@main.route('/box/<hash>/shuffle_box')
def shuffle_box(hash):
    data = Box.shuffle_box(box_hash=hash)
    if data:
        flash(data)
    return redirect(url_for('.box', hash=hash))
