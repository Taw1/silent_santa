import random

from . import db, login_manager
from.email import send_email
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128))
    email = db.Column(db.String(128), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)
    box_host = db.relationship("Box", backref="user")

    @property
    def password(self):
        raise AttributeError('not readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True


class Box(db.Model):
    __tablename__ = 'boxes'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    allow_suggestions = db.Column(db.Boolean, default=False)
    money_rest = db.Column(db.Integer, default=0)
    shuffled = db.Column(db.Boolean, default=False)
    box_hash = db.Column(db.String(128))
    card_box = db.relationship('Card', backref="box")

    @staticmethod
    def create_box(user_id, user_email, allow_suggestions, money_rest, suggestions=''):
        b = Box(user_id=user_id,
                allow_suggestions=allow_suggestions,
                box_hash=generate_password_hash(user_email)[-10:],
                money_rest=money_rest)
        db.session.add(b)
        Card.create_card(box_hash=b.box_hash,
                         email=user_email,
                         username=User.query.filter_by(id=user_id).one().username,
                         suggestions=suggestions)
        return b.box_hash

    @staticmethod
    def shuffle_box(box_hash):
        box = Box.query.filter_by(box_hash=box_hash).one()
        if box.shuffled:
            return 'Already shuffled'
        cards = Card.query.filter_by(box_id=box.id).all()
        secret_santa_emails = [card.email for card in cards]
        if len(cards) % 2 != 0:
            return 'Need even cards query'
        for card in cards:
            card_email = secret_santa_emails.pop(secret_santa_emails.index(card.email)) if card.email in secret_santa_emails else None
            card.secret_santa_email = secret_santa_emails.pop(secret_santa_emails.index(random.choice(secret_santa_emails)))
            if card_email:
                secret_santa_emails.append(card_email)
            db.session.add(card)
        box.shuffled = True
        db.session.add(box)
        for card in Card.query.filter_by(box_id=box.id).all():
            send_email(card.email,
                       'Shuffle results',
                       'email/shuffle_result',
                       box=box,
                       card=Card.query.filter_by(email=card.secret_santa_email).filter_by(box_id=box.id).one())
        return 'Successfully shuffled. Emails with information was successfully sent.'


class Card(db.Model):
    __tablename__ = 'cards'
    id = db.Column(db.Integer, primary_key=True)
    box_id = db.Column(db.Integer, db.ForeignKey('boxes.id'))
    email = db.Column(db.String(128), index=True, nullable=False)
    username = db.Column(db.String(128), nullable=False)
    suggestions = db.Column(db.Text)
    secret_santa_email = db.Column(db.String(128))

    @staticmethod
    def create_card(box_hash, email, username, suggestions=''):
        b = Box.query.filter_by(box_hash=box_hash).one()
        if Card.query.filter_by(box_id=b.id).filter_by(email=email).first():
            return 'User email ---- {} ---- already in box'.format(email)
        c = Card(box_id=b.id, email=email, username=username, suggestions=suggestions)
        db.session.add(c)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
